//variables
let playing = false;
let playerOne=false;
let playerTwo=false;
let isComputer = false;
let isTwoPlayers = false;

let player1Positions=[];
let player2Positions=[];

//Required Elements
let startreset = document.getElementById("startreset");
let boxes= document.querySelector("#game-area");

//Events
document.getElementById("mode-1").addEventListener('click',showGameArea);
document.getElementById("mode-1").addEventListener('click',twoPlayers);
document.getElementById("mode-2").addEventListener('click',showGameArea);
document.getElementById("mode-2").addEventListener('click',vsComputer);
startreset.addEventListener('click',startResetGame);
boxes.addEventListener('click',generateSymbol);
let box = document.querySelectorAll(".box");

//Helper Function
function setText(id, text) {
    document.getElementById(id).innerHTML = text;
}
function show(id) {
    document.getElementById(id).style.display = 'block';
}
function hide(id) {
    document.getElementById(id).style.display = 'none';
}
//default
show("modes");
hide("startreset");

function showGameArea(){
    hide("modes");
    show("startreset");
}

function twoPlayers(){
    isTwoPlayers = true;
    isComputer = false;
      setText("you","<h4 class='text-center'>Player 1</h4>");
  setText("computer","<h4 class='text-center'>Player 2</h4>");
}
function vsComputer(e){
    isComputer = true;
    isTwoPlayers = false;
  setText("computer","<h4 class='text-center'>Computer</h4>");
  setText("you","<h4 class='text-center'>you</h4>");
}

function computerChance(e){
     if ((player1Positions.length + player2Positions.length < 9)) {

        let boxNumber;

        do {

            boxNumber = String(Math.round((1 + Math.random() * 8)));
        } while (document.getElementById("box-" + boxNumber).innerHTML != '');
         
        setText("box-" + boxNumber, "o");
      
        player2Positions.push("box-"+boxNumber);
         
    }
}

function startResetGame(e){
    player1Positions=[];
    player2Positions=[];
   if (playing === true) {
        //game is ON and you want to reset
        setText("startreset", "Start Game");
       hide("game-over");
       setText("s-1","0");
        setText("s-2","0");
        show("modes");
        hide("startreset");
       for (let i = 1; i < 10; i++)
            setText("box-" + i, '');    
       
   }else{
       //game is off and you want to ON
       setText("startreset","Reset Game");
       hide("game-over");
        for (let i = 1; i < 10; i++)
            setText("box-" + i, '');

       playerOne=true;

   }
     playing = !playing;
}

function generateSymbol(e){
    if(playerOne && e.target.innerHTML==="" && playing && isTwoPlayers){
       e.target.innerHTML="x";
        player1Positions.push(e.target.getAttribute("id"));
        playerOne = !playerOne;
        playerTwo = !playerTwo;
        setText("game-over","<h1 class = 'm-b-2'>Game Over</h1><h2>Player 1</h2><h3 class = 'm-heading winner'>won!</h3>");
        checkConditions(e);
    }
    else if(playerTwo && e.target.innerHTML==="" && playing && isTwoPlayers){
        e.target.innerHTML="o";
         player2Positions.push(e.target.getAttribute("id"));
        playerOne = !playerOne;
        playerTwo = !playerTwo;
        setText("game-over","<h1 class = 'm-b-2'>Game Over</h1><h2>Player 2</h2><h3 class = 'm-heading winner'>won!</h3>");
        checkConditions(e);
    }
   else if(isComputer){
        if (playerOne && e.target.innerHTML === '' && playing) {
            
            e.target.innerHTML = 'x';
            player1Positions.push(e.target.getAttribute("id"));
            
            
            setTimeout(computerChance,500);
                 setText("game-over","<h1 class = 'm-b-2'>Game Over</h1><h2>you</h2><h3 class = 'm-heading winner'>won!</h3>"); 
            setTimeout(checkConditions,500);
            
        }
    }

}

function checkConditions(e){
    
    if((player1Positions.includes("box-1") && player1Positions.includes("box-2") && player1Positions.includes("box-3") ) ||
       (player1Positions.includes("box-1") && player1Positions.includes("box-4") && player1Positions.includes("box-7") ) ||
       (player1Positions.includes("box-1") && player1Positions.includes("box-5") && player1Positions.includes("box-9") ) ||
       (player1Positions.includes("box-7") && player1Positions.includes("box-5") && player1Positions.includes("box-3") ) ||
       (player1Positions.includes("box-2") && player1Positions.includes("box-5") && player1Positions.includes("box-8") ) ||
       (player1Positions.includes("box-4") && player1Positions.includes("box-5") && player1Positions.includes("box-6") ) ||
       (player1Positions.includes("box-8") && player1Positions.includes("box-7") && player1Positions.includes("box-9") ) ||
       (player1Positions.includes("box-3") && player1Positions.includes("box-6") && player1Positions.includes("box-9") )
       
      ){
                if(isComputer){
              setText("game-over","<h1 class = 'm-b-2'>Game Over</h1><h2>you</h2><h3 class = 'm-heading winner'>won!</h3>");
        }
        show("game-over");
        for (let i = 1; i < 10; i++)
            setText("box-" + i, '');

        setText("s-1","1");
        
    }else if((player2Positions.includes("box-1") && player2Positions.includes("box-2") && player2Positions.includes("box-3")) ||
       (player2Positions.includes("box-1") && player2Positions.includes("box-4") && player2Positions.includes("box-7") ) ||
       (player2Positions.includes("box-1") && player2Positions.includes("box-5") && player2Positions.includes("box-9") ) ||
       (player2Positions.includes("box-7") && player2Positions.includes("box-5") && player2Positions.includes("box-3") ) ||
       (player2Positions.includes("box-2") && player2Positions.includes("box-5") && player2Positions.includes("box-8") ) ||
       (player2Positions.includes("box-4") && player2Positions.includes("box-5") && player2Positions.includes("box-6") ) ||
       (player2Positions.includes("box-8") && player2Positions.includes("box-7") && player2Positions.includes("box-9") ) ||
        (player2Positions.includes("box-3") && player2Positions.includes("box-6") && player2Positions.includes("box-9") )
       ){
        if(isComputer){
              setText("game-over","<h1 class = 'm-b-2'>Game Over</h1><h2>you</h2><h3 class = 'm-heading looser'>lose!</h3>");
        }
        show("game-over");
        for (let i = 1; i < 10; i++)
            setText("box-" + i, '');

        setText("s-2","1");
    }
    else if(player1Positions.length + player2Positions.length == 9){ 
         setText("game-over","<h1 class = 'm-b-2'>Game Over</h1><h2>Draw!!!</h2>");
        show("game-over");
        
        for (let i = 1; i < 10; i++)
            setText("box-" + i, '');
    }
}

    
    